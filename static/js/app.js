String.prototype.capitalize = function () {
    return this.toLowerCase().split(' ').map(function (i) {
        if (i.length > 1) {
            return i.charAt(0).toUpperCase() + i.substr(1);
        } else {
            return i;
        }
    }).join(' ');
};

var socket = io.connect("http://127.0.0.1:8080/chat");

var app = new Vue ({
    el: '#id_app',
    data: {
        page_title: "Test Executor",
        page_heading: "Test Executor",
        pageHeading: "page-heading",
		listTable: "list-table",
        api_url_domain: "http://127.0.0.1:8081/api/v1/",
		polling_url: null,
		detail_polling_test_uuid: null,
		show_perform_detail_polling: false,
        test_headers: {
            username: "User Name",
            test_uuid: "Test UUID",
			test_dir: "Test Directory",
			test_env: "Test Environment",
			status: "Status"
        },
		test_env_options: [
			{
				name: "Select Environment",
				env_id: -1,
                locked: false
			}
		],
        layout_list_span: 24,
        layout_detail_span: 1,
        show_layout_detail: false,
        tests: [],
        confirm_dialog_visible: false,
        confirm_dialog_message: "",
        show_previous_button: false,
        show_next_button: false,
        previous_url: null,
        next_url: null,
        total_tests_count: 0,
		form: {
			username: "",
			test_env: -1,
			test_dir: "",
            get_serialized_data: function() {
			    return {
			        username: this.username,
                    test_env: this.test_env,
                    test_dir: this.test_dir
                }
            },
            clear_form: function() {
			    this.username = "";
			    this.test_env = -1;
			    this.test_dir = "";
            },
			is_valid: function() {
				return this.username != "" && this.test_env != "" && this.test_dir != "";
			},
			get_message: function() {
				msg = "";
				if(this.username == "") {
					msg += "* User name required\n ";
				}
				if(this.test_env == "") {
					msg += "* Test env required\n ";
				}
				if(this.test_dir == "") {
					msg += "* Test directory required\n";
				}
				return msg;
			}
		},
        test_detail: {

        }
    },
    methods:{
        call_ajax: function(url, method, data, success_callback, error_callback) {
            if(method == "GET") {
                axios.get(url).then(success_callback, error_callback);
            }
            else if(method == "POST"){
                axios.post(url, data).then(success_callback).catch(error_callback);
            }
        },
        handle_confirm_dialog_close: function() {

        },
        fetch_all_tests: function (action) {
            var $this = this;
			if(action != "polling") {
				$this.tests = [];
			}
            
            var url = this.api_url_domain + "tests/";
            if(typeof action != "undefined") {
                if(action == "previous" && $this.previous_url != null) {
                    url = $this.previous_url;
                }
                else if(action == "next" && $this.next_url != null) {
                    url = $this.next_url;
                }
				else if(action == "polling") {
					if($this.polling_url != null) {
						url = $this.polling_url;
					}				
				}
            }
			$this.polling_url = url;
            this.call_ajax(url, "GET", {}, function(response){
                //console.log(response);
                var results = response.data.results;
                $this.total_tests_count = response.data.count;
				if(action == "polling") {
					if(results.length == $this.tests.length) {
						var status_map = {};
						for(var i = 0 ; i < results.length; i++) {
							var result_item = results[i];
							status_map[result_item.test_uuid] = result_item.test_status;
						}
						for(var i = 0 ; i < $this.tests.length ; i++ ) {
							var test_item = $this.tests[i];
							var status = test_item.status;
							if(status_map.hasOwnProperty(test_item.test_uuid)) {
								status = status_map[test_item.test_uuid];
							}
							$this.tests[i].status = status;
						}
					}
					else {
						$this.tests = [];
						for(var i = 0 ; i < results.length; i++) {
							var result_item = results[i];
							$this.tests.push(result_item);
						}
					}					
				}
				else {
					$this.previous_url = response.data.previous;
					$this.next_url = response.data.next;
					if(response.data.previous != null) {
						$this.show_previous_button = true;
					}
					else {
						$this.show_previous_button = false;
					}
					if(response.data.next != null) {
						$this.show_next_button = true;
					}
					else {
						$this.show_next_button = false;
					}
					for(var i = 0 ; i < results.length; i++) {
						var result_item = results[i];
						$this.tests.push(result_item);
					}
				}
                //console.log($this.show_previous_button);
            }, function(error){

            });
        },
        handle_previous_click: function() {
            var $this = this;
            $this.fetch_all_tests("previous");
        },
        handle_next_click: function() {
            var $this = this;
            $this.fetch_all_tests("next");
        },
        fetch_test_env_list: function() {
            var $this = this;
            var url = this.api_url_domain + "test-env/?limit=100&offset=0";
            this.call_ajax(url, "GET", {}, function(response){
                //console.log(response);
                var results = response.data.results;
                for(var i = 0; i < results.length; i++) {
                    $this.test_env_options.push(results[i]);
                }

            }, function(error){

            });
        },
		submit_test: function() {
            var $this = this;
			if(!$this.form.is_valid()) {
				$this.confirm_dialog_visible = true;
                $this.confirm_dialog_message = $this.form.get_message();
				return false;
			}
            var url = this.api_url_domain + "tests/";
            //console.log($this.form_data);
            var post_data = $this.form.get_serialized_data();
            //console.log(post_data);
            this.call_ajax(url, "POST", post_data, function(response){
                //console.log(response);
                if(response.status == 201) {
                    $this.confirm_dialog_visible = true;
                    $this.confirm_dialog_message = "Test Submitted Successfully";
                    $this.form.clear_form();
                }
                else if(response.status == 200) {
                    $this.confirm_dialog_visible = true;
                    $this.confirm_dialog_message = "Test Submit Failed. Reason: " + response.data.message;
                    $this.form.clear_form();
                }
                else {
                    $this.confirm_dialog_visible = true;
                    $this.confirm_dialog_message = "Test Submit Failed. Reason: " + response.data.message;
                }
            }, function(error){
                $this.confirm_dialog_visible = true;
                $this.confirm_dialog_message = "Test Submit Failed. Server Error.";
            });
            return false;
        },
        fetch_test_details: function(test_uuid, success_callback, error_callback) {
            var url = this.api_url_domain + "tests/" + test_uuid + "/";
			this.detail_polling_test_uuid = test_uuid;
            this.call_ajax(url, "GET", {}, function(response){
                //console.log(response.status);
                if(response.status == 200) {
                    success_callback(response.data);
                }
                else {
                   error_callback();
                }

            }, function(error){

            });
        },
        handle_detail_click: function (test_uuid) {
            var $this = this;
            $this.fetch_test_details(test_uuid, function (data) {
                $this.test_detail = data;
                $this.layout_list_span = 16;
                $this.layout_detail_span = 8;
                $this.show_layout_detail = true;
				//Activate the detail polling.
				$this.show_perform_detail_polling = true;
            }, function () {
                $this.test_detail = {};
                $this.layout_list_span = 24;
                $this.layout_detail_span = 1;
                $this.show_layout_detail = false;
				$this.show_perform_detail_polling = false;
                $this.confirm_dialog_visible = true;
                $this.confirm_dialog_message = "Test Details Fetch Failed.";
            });
        },
		perform_details_polling: function() {
			var $this = this;
			if($this.show_perform_detail_polling && $this.detail_polling_test_uuid != null) {
				$this.fetch_test_details($this.detail_polling_test_uuid, function (data) {
					$this.test_detail = data;
					$this.layout_list_span = 16;
					$this.layout_detail_span = 8;
					$this.show_layout_detail = true;
				}, function () {
					$this.test_detail = {};
					$this.layout_list_span = 24;
					$this.layout_detail_span = 1;
					$this.show_layout_detail = false;
				});
			}
		},
        handle_detail_close: function () {
            var $this = this;
            $this.test_detail = {};
            $this.layout_list_span = 24;
            $this.layout_detail_span = 1;
            $this.show_layout_detail = false;
        }
    },
    mounted: function () {
        this.fetch_test_env_list();
        this.fetch_all_tests();
    },
	ready: function() {
		var $this = this;
		setInterval(function () {
		  $this.fetch_all_tests("polling");
		}.bind(this), 1000);
		
		setInterval(function() {
			$this.perform_details_polling();
		}.bind(this), 500);
		
	}
});
